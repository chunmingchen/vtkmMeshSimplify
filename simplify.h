#include <vtkm/cont/DataSet.h>

extern vtkm::Float64 vertex_clustering_time;

vtkm::cont::DataSet simplify(vtkm::cont::DataSet ds, const double *bounds, int grid_division);
