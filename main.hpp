
#include <cstdio>
#include <iostream>
#include <vector>
#include <map>
#include <iomanip>

#if VTKM_DEVICE_ADAPTER==VTKM_DEVICE_ADAPTER_TBB
#include <tbb/task_scheduler_init.h>
#endif

#include "vtkSmartPointer.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkObjectFactory.h"
#include "vtkXMLPolyDataReader.h"
#include "vtkAxes.h"
#include "vtkAxesActor.h"
#include "vtkImageData.h"
#include <vtkExtractEdges.h>
#include "vtkVector.h"
#include "vtkTriangle.h"
#include "vtkTriangleFilter.h"
#include "vtkPoints.h"
#include "vtkCellArray.h"
#include "vtkQuadricClustering.h"
#include "vtkPLYReader.h"
#include "vtkPolyDataWriter.h"
#include "vtkQuadric.h"
#include "vtkPolyDataReader.h"
#include "vtkSmartPointer.h"
#include "vtkDataSetMapper.h"
#include <vtkProperty.h>

#include "vtkPolyDataMapper.h"
#include "vtkRenderWindow.h"
#include "vtkActor.h"
#include "vtkRenderer.h"
#include "vtkMatrix4x4.h"
#include "vtkMatrix3x3.h"

#include <vtkm/cont/Timer.h>
#include <vtkm/cont/testing/MakeTestDataSet.h>
#include "vtk_vtkm.h"

#include "simplify.h"
//#include "vtkm/worklet/VertexClustering.h"
#include "cp_time.h"

#if (VTKM_DEVICE_ADAPTER==VTKM_DEVICE_ADAPTER_SERIAL)
#pragma message ("Using Serial")
#elif (VTKM_DEVICE_ADAPTER==VTKM_DEVICE_ADAPTER_TBB)
#pragma message ("Using TBB")
#elif (VTKM_DEVICE_ADAPTER==VTKM_DEVICE_ADAPTER_CUDA)
#pragma message ("Using CUDA")
#else
#pragma message ("Device Unknown")
#endif

using namespace std;
//using namespace boost::numeric;

#define vsp_new(type, name) vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

vtkRenderer *ren;
vtkRenderWindow *renWin;

bool bShowGrids = false;
#ifdef PROFILING
bool bShowOriginal = false;
#else
bool bShowOriginal = true;
#endif
bool bShowOutput = false;
bool bUseVTKSimplify = false;

/// for mesh simplification
vtkSmartPointer<vtkPolyData> data;
vtkSmartPointer<vtkPolyData> output_data;

double origin[3];
int xdim, ydim, zdim;
void set_origin_and_dim();
double grid_width = 0.015625;
//float one_over_grid_width = 1./grid_width;

int grid_division = 30;


/// determine grid resolution for clustering
void set_origin_and_dim()
{
    double *bounds = data->GetBounds();
    double res[3];
    for (int i=0; i<3; i++)
        res[i] = (bounds[i*2+1]-bounds[i*2])/grid_division;
    grid_width = std::max(res[0], std::max(res[1], res[2]));


    double one_over_grid_width = 1. / grid_width;

    xdim = ceil((bounds[1]-bounds[0])*one_over_grid_width);
    ydim = ceil((bounds[3]-bounds[2])*one_over_grid_width);
    zdim = ceil((bounds[5]-bounds[4])*one_over_grid_width);
    if (0) {
        origin[0] = bounds[0];
        origin[1] = bounds[2];
        origin[2] = bounds[4];
    }else {
        origin[0] = (bounds[1]+bounds[0])*0.5 - grid_width*(xdim)*.5;
        origin[1] = (bounds[3]+bounds[2])*0.5 - grid_width*(ydim)*.5;
        origin[2] = (bounds[5]+bounds[4])*0.5 - grid_width*(zdim)*.5;
    }
}

/// the vtk version
double vtk_simplify(vtkSmartPointer<vtkPolyData> data, vtkSmartPointer<vtkPolyData> &output_data_)
{

    set_origin_and_dim();   // compute xdim, ydim, zdim

    vsp_new(vtkQuadricClustering, filter);
    filter->SetInputData(data);
    filter->SetNumberOfXDivisions(xdim);
    filter->SetNumberOfYDivisions(ydim);
    filter->SetNumberOfZDivisions(zdim);
    filter->AutoAdjustNumberOfDivisionsOff();

    vtkm::cont::Timer<> timer;

    filter->Update();

    double t = timer.GetElapsedTime();
    cout << "Time (s): " << t << endl;

    output_data = vtkSmartPointer<vtkPolyData>::New();
    output_data->DeepCopy(filter->GetOutput());

    cout << "Number of output points: " << output_data->GetNumberOfPoints() << endl;
    cout << "Number of output cells: " << output_data->GetNumberOfCells() << endl;

    filter->GetDivisionOrigin(origin);
    double *spacing = filter->GetDivisionSpacing();
    printf ("Spacing: %lf %lf %lf\n", spacing[0], spacing[1], spacing[2]);
    return t;
}

void draw()
{
    ren->RemoveAllViewProps();

    // draw data
    if (bShowOriginal)
    {
        vsp_new(vtkPolyDataMapper, polymapper);
        polymapper->SetInputData(data);

        vsp_new(vtkActor, polyactor);
        polyactor->SetMapper(polymapper);

        ren->AddActor(polyactor);
    }

    // draw data
    if (bShowOutput)
    {
        vsp_new(vtkPolyDataMapper, polymapper);
        polymapper->SetInputData(output_data);

        vsp_new(vtkActor, polyactor);
        polyactor->SetMapper(polymapper);

        ren->AddActor(polyactor);
    }


    // axes
    vsp_new(vtkAxesActor, axes);
    ren->AddActor(axes);

    if (bShowGrids)
    {
        cout << "grid width=" << grid_width << endl;
        cout << "grid_division = " << grid_division << endl;
        set_origin_and_dim();

        vsp_new(vtkImageData, image);
        image->SetDimensions(xdim+1, ydim+1, zdim+1); // grids are larger than cells by one in each dimension
        image->SetOrigin(origin[0], origin[1], origin[2]);
        image->SetSpacing(grid_width, grid_width, grid_width);

        vsp_new(vtkDataSetMapper, mapper);
        mapper->SetInputData(image);

        vsp_new(vtkActor, polyactor);
        polyactor->SetMapper(mapper);
        polyactor->GetProperty()->SetRepresentationToWireframe();

        ren->AddActor(polyactor);
    }

    ren->SetBackground(0,0,.5); // Background color
    renWin->Render();
}

// Define interaction style
class KeyPressInteractorStyle : public vtkInteractorStyleTrackballCamera
{
  public:
    static KeyPressInteractorStyle* New();
    vtkTypeMacro(KeyPressInteractorStyle, vtkInteractorStyleTrackballCamera);

    virtual void OnKeyPress()
    {
      // Get the keypress
      vtkRenderWindowInteractor *rwi = this->Interactor;
      std::string key = rwi->GetKeySym();

      // Output the key that was pressed
      std::cout << "Pressed " << key << std::endl;

      // Handle an arrow key
      if(key == "Up")
        {
        std::cout << "The up arrow was pressed." << std::endl;
        }

      // Handle a "normal" key
      if(key == "s")
        {
          cout << "Input number of points: " << data->GetNumberOfPoints() << ", number of triangles: " << data->GetNumberOfCells() << endl;
          if (bUseVTKSimplify) {
              vtk_simplify(data, output_data);

          } else {
              vtkm::cont::DataSet ds = vtk2vtkm<vtkm::Float32>(data), ds_out;
              ds_out = simplify(ds, data->GetBounds(), grid_division);
              //ds_out = vtkm::worklet::VertexClustering<VTKM_DEFAULT_DEVICE_ADAPTER_TAG>().run(ds, data->GetBounds(), grid_division);
              output_data = vtkm2vtk<vtkm::Float32>(ds_out);
            }
          bShowOriginal = false;
          bShowOutput = true;
          draw();
        }
      if (key == "o" )
      {
          bShowOriginal = !bShowOriginal ;
          bShowOutput = !bShowOutput;
          draw();
      }
      if (key=="minus") {
          if (grid_division>1)
            grid_division /= 2;
          cout << "grid division =" << grid_division << endl;
          if (bShowGrids)
            draw();
      }
      if (key=="plus") {
          grid_division *= 2;
          cout << "grid division =" << grid_division << endl;
          if (bShowGrids)
            draw();
      }
      if (key=="g") {
          bShowGrids = ! bShowGrids;
          draw();
      }
      if (key=="v") {
          bUseVTKSimplify = ! bUseVTKSimplify;
          cout << "Use vtk = " << bUseVTKSimplify << endl;
      }
      if (key=="z") {
          cout << "Saving to output.vtk ..." << endl;
          vsp_new(vtkPolyDataWriter , writer);
          writer->SetFileName("output.vtk");
          writer->SetInputData(output_data);
          writer->Write();
      }

      // Forward events
      vtkInteractorStyleTrackballCamera::OnKeyPress();
    }

};
vtkStandardNewMacro(KeyPressInteractorStyle);

void load_input(const char *filename, int grid_division)
{
    printf("Loading file: %s\n", filename);

    int len = strlen(filename);
    const char *ext = filename + (len-3);

    vtkm::cont::Timer<> timer;
    vsp_new(vtkPolyData, data_in);
    if (strcasecmp(ext, "ply")==0)
    {
        vsp_new(vtkPLYReader, reader);
        reader->SetFileName(filename);
        reader->Update();
        data_in->DeepCopy(reader->GetOutput());
    } else if (strcasecmp(ext, "vtp")==0){
        vsp_new(vtkXMLPolyDataReader,reader);
        reader->SetFileName(filename);
        reader->Update();
        data_in->DeepCopy(reader->GetOutput());
    } else if (strcasecmp(ext, "vtk")==0){
        vsp_new(vtkPolyDataReader,reader);
        reader->SetFileName(filename);
        reader->Update();
        data_in->DeepCopy(reader->GetOutput());
    }
    cout << "VTK disk loading time: " << timer.GetElapsedTime() << endl;

    if (data_in->GetNumberOfPoints()==0) {
        printf("File not loaded.  Use testing datset\n");
        auto ds = vtkm::cont::testing::MakeTestDataSet().Make3DExplicitDataSetCowNose();
        data_in = vtkm2vtk<float>( ds );
    }

    // triangulate
    vsp_new(vtkTriangleFilter, tri);
    tri->SetInputData(data_in);
    tri->Update();

    data = vtkSmartPointer<vtkPolyData>::New();
    data->DeepCopy(tri->GetOutput());
}

void benchmark()
{
    int c;
    const int tests = 10;
    const int tests2 = 3;
    double t1=0, t2=0;

    vtkm::cont::Timer<> timer;
    vtkm::cont::DataSet ds = vtk2vtkm<vtkm::Float32>(data),
        ds_out;
    cout << "vtk->vtkm Data convertion time: " << timer.GetElapsedTime() << endl;

    cout << "vtkm :" << endl;
    for (c=0; c<tests+1; c++){
        ds_out = simplify(ds, data->GetBounds(), grid_division);
        if (c>0) {
            t1 += vertex_clustering_time;
        }        
#ifdef SAVE_OUTPUT_ON
        if (c==0) {
          output_data = vtkm2vtk<vtkm::Float32>(ds_out);
          cout << "** Saving to output.vtk ... **" << endl;
          vsp_new(vtkPolyDataWriter , writer);
          writer->SetFileName("output.vtk");
          writer->SetFileTypeToBinary();
          writer->SetInputData(output_data);
          writer->Write();
        }
#endif
    }
    cout << "Average vtkm: " << t1/tests << endl;

    cout << "vtk: " << endl;
    set_origin_and_dim();
    for (c=0; c<tests2+1; c++)
    {
        double t= vtk_simplify(data, output_data);
        if (c>0)
            t2 += t;
#ifdef SAVE_OUTPUT_ON
        if (c==0) {
          cout << "** Saving to output_vtk.vtk ... **" << endl;
          vsp_new(vtkPolyDataWriter , writer);
          writer->SetFileName("output_vtk.vtk");
          writer->SetFileTypeToBinary();
          writer->SetInputData(output_data);
          writer->Write();
        }
#endif
    }
    cout << "Average vtkm: " << t1/tests << endl;
    cout << "Average vtk: " << t2/tests2 << endl;
}


int main( int argc, char **argv )
{
    cout << "Input arguments: <vtp/vtk/ply file> <grid_division=30> <max cores>\n";
#if VTKM_DEVICE_ADAPTER==VTKM_DEVICE_ADAPTER_TBB
    int cores = tbb::task_scheduler_init::default_num_threads();
    if (argc>3)
        cores = atoi(argv[3]);
    tbb::task_scheduler_init init(cores);  //!!!!!!!!!!!!!!!!
    cout << "Max threads to use: "  << cores << endl;
#endif
    if (argc>2)
        grid_division = atoi(argv[2]);

    if (argc>1)
        load_input(argv[1], grid_division);
    else
        load_input(DATA_FILE, grid_division);

#ifdef PROFILING
    benchmark();
    exit(0);
#endif
    printf("Keys:\n"
           "g: Toggle showing grids\n"
           "+/-: Increase/decrease grid size by 2\n"
           "<<< s: Simplify mesh >>>\n"
           "o: Toggle showing orginal model or simplified model\n"
           "v: Toggle using VTK simplification filter\n"
           "z: Save output data\n"
           );

    // Visualize
    vsp_new(vtkRenderer, ren);
    vsp_new(vtkRenderWindow, renWin);
    ::ren = ren.GetPointer();
    ::renWin = renWin.GetPointer();

    renWin->AddRenderer(ren);
    renWin->SetSize(800,600);

    vsp_new(vtkRenderWindowInteractor, renderWindowInteractor );
    renderWindowInteractor->SetRenderWindow(renWin);

    vsp_new(KeyPressInteractorStyle, style);
    style->SetCurrentRenderer(ren);
    renderWindowInteractor->SetInteractorStyle(style);

    draw();

    ren->ResetCamera();

    renWin->Render();

    renderWindowInteractor->Start();
    return EXIT_SUCCESS;

}
