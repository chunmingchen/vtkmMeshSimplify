#define BOOST_SP_DISABLE_THREADS
#include <vector>
#include <iostream>

#include <vtkm/worklet/PointElevation.h>
#include <vtkm/worklet/DispatcherMapField.h>
#include <vtkm/cont/DeviceAdapter.h>

#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/testing/Testing.h>
#include <vtkm/cont/testing/MakeTestDataSet.h>

#include <vtkm/worklet/VertexClustering.h>
#include <vtkm/cont/Timer.h>
#include "simplify.h"

#if (VTKM_DEVICE_ADAPTER==VTKM_DEVICE_ADAPTER_SERIAL)
#pragma message ("Using Serial")
#elif (VTKM_DEVICE_ADAPTER==VTKM_DEVICE_ADAPTER_TBB)
#pragma message ("Using TBB")
#elif (VTKM_DEVICE_ADAPTER==VTKM_DEVICE_ADAPTER_CUDA)
#pragma message ("Using CUDA")
#else
#pragma message ("Device Unknown")
#endif

/////////////////////////////////////////////
vtkm::Float64 vertex_clustering_time=0;

template<typename T, int N>
vtkm::cont::ArrayHandle<T> copyFromVec( vtkm::cont::ArrayHandle< vtkm::Vec<T, N> > const& other)
{
    const T *rmem = reinterpret_cast< const T *>(& *other.GetPortalConstControl().GetIteratorBegin());
    std::vector<T> vmem (rmem, rmem+other.GetNumberOfValues()*N);
    vtkm::cont::ArrayHandle<T> mem = vtkm::cont::make_ArrayHandle(vmem);
    vtkm::cont::ArrayHandle<T> result;
    vtkm::cont::DeviceAdapterAlgorithm<VTKM_DEFAULT_DEVICE_ADAPTER_TAG>::Copy(mem,result);
    return result;
}

template<typename T, typename StorageTag>
vtkm::cont::ArrayHandle<T> copyFromImplicit( vtkm::cont::ArrayHandle<T, StorageTag> const& other)
{
  vtkm::cont::ArrayHandle<T> result;
  vtkm::cont::DeviceAdapterAlgorithm<VTKM_DEFAULT_DEVICE_ADAPTER_TAG>::Copy(other, result);
  return result;
}

vtkm::cont::DataSet RunVertexClustering(vtkm::cont::DataSet &ds, const vtkm::Float64 bounds[6], int nDivisions)
{
  typedef vtkm::Vec<vtkm::Float32,3>  PointType;

  boost::shared_ptr<vtkm::cont::CellSet> scs = ds.GetCellSet(0);
  vtkm::cont::CellSetExplicit<> *cs =
      dynamic_cast<vtkm::cont::CellSetExplicit<> *>(scs.get());

  vtkm::cont::ArrayHandle<PointType> pointArray = ds.GetField("xyz").GetData().CastToArrayHandle<PointType, VTKM_DEFAULT_STORAGE_TAG>();
  vtkm::cont::ArrayHandle<vtkm::Id> pointIdArray = cs->GetNodeToCellConnectivity().GetConnectivityArray();
  vtkm::cont::ArrayHandle<vtkm::Id> cellToConnectivityIndexArray = cs->GetNodeToCellConnectivity().GetCellToConnectivityIndexArray();

  vtkm::cont::ArrayHandle<PointType> output_pointArray ;
  vtkm::cont::ArrayHandle<vtkm::Id3> output_pointId3Array ;

  // run
  vtkm::cont::Timer<> timer;
  vtkm::worklet::VertexClustering<VTKM_DEFAULT_DEVICE_ADAPTER_TAG>().run(pointArray, pointIdArray, cellToConnectivityIndexArray,
                                                       bounds, nDivisions,
                                                       output_pointArray, output_pointId3Array);
  vertex_clustering_time = timer.GetElapsedTime();

  vtkm::cont::DataSet new_ds;

  new_ds.AddField(vtkm::cont::Field("xyz", 0, vtkm::cont::Field::ASSOC_POINTS, output_pointArray));
  new_ds.AddCoordinateSystem(vtkm::cont::CoordinateSystem("xyz"));

  int cells = output_pointId3Array.GetNumberOfValues();
  if (cells > 0)
  {
    //typedef typename vtkm::cont::ArrayHandleConstant<vtkm::Id>::StorageTag ConstantStorage;
    //typedef typename vtkm::cont::ArrayHandleImplicit<vtkm::Id, CounterOfThree>::StorageTag CountingStorage;
    typedef vtkm::cont::CellSetExplicit<> Connectivity;

    boost::shared_ptr< Connectivity > new_cs(
        new Connectivity("cells", 0) );

      new_cs->GetNodeToCellConnectivity().Fill(
        copyFromImplicit(vtkm::cont::make_ArrayHandleConstant<vtkm::Id>(vtkm::VTKM_TRIANGLE, cells)),
        copyFromImplicit(vtkm::cont::make_ArrayHandleConstant<vtkm::Id>(3, cells)),
        copyFromVec(output_pointId3Array)
            );

    new_ds.AddCellSet(new_cs);
  }

  return new_ds;
}

vtkm::cont::DataSet simplify(vtkm::cont::DataSet ds, const double *bounds, int grid_division)
{
  return RunVertexClustering(ds, bounds, grid_division);
}
