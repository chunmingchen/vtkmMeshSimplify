#include <iostream>

#include <vtkm/cont/DeviceAdapter.h>

#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/ArrayHandleCounting.h>
#include <vtkm/cont/ArrayHandlePermutation.h>

#include <vtkm/cont/DynamicArrayHandle.h>
#include <vtkm/cont/Timer.h>
#include <vtkm/Pair.h>
#include <vtkm/worklet/DispatcherMapField.h>
#include <vtkm/worklet/WorkletMapField.h>
#include <vtkm/cont/DeviceAdapterAlgorithm.h>
#include <vtkm/cont/ArrayHandleConstant.h>
#include <vtkm/cont/ArrayHandleCompositeVector.h>
//#include <vtkm/cont/DeviceAdapterSerial.h>


namespace vtkm{
namespace worklet{


#if 0
// added to vtkm: Pair.h
class ZipAdd{
public:
    template<typename T, typename U>
    vtkm::Pair<T, U> operator()(const vtkm::Pair<T, U>& a, const vtkm::Pair<T, U> &b)const
    {
        return vtkm::Pair<T,U>(a.first+b.first, a.second + b.second);
    }
};
#endif

// debugging:
template <class T, vtkm::Id N>
std::ostream &operator<<(std::ostream &cout, const vtkm::Vec<T, N> &vec)
{
    cout << "<" << vec[0];
    for (int i=1; i<N; i++)
            cout << ", " << vec[i] ;
    cout << ">";
    return cout;
}
template <class T, class U>
std::ostream &operator<<(std::ostream &cout, const vtkm::Pair<T, U> &pair)
{
    cout << "<" << pair.first << "," << pair.second << ">";
    return cout;
}
// debug
template<class T>
void print_array(const char *msg, const T &array)
{
#if 0
    cout << msg << "(" << array.GetNumberOfValues() << ")";
    for (int i=0; i<array.GetNumberOfValues(); i++)
    {
        cout << array.GetPortalConstControl().Get(i) << " ";
    }
    cout << endl;
#endif
}



// TODO: custom Less()
template <class KeyType, class ValueType, class DeviceAdapter = VTKM_DEFAULT_DEVICE_ADAPTER_TAG>
void AverageByKey( const vtkm::cont::ArrayHandle<KeyType> &keyArray,
                   const vtkm::cont::ArrayHandle<ValueType> &valueArray,
                   vtkm::cont::ArrayHandle<KeyType> &outputKeyArray,
                   vtkm::cont::ArrayHandle<ValueType> &outputValueArray)
{
    vtkm::cont::Timer<> timer;

    vtkm::cont::ArrayHandle<ValueType> sumArray;
    vtkm::cont::ArrayHandle<KeyType> keyArraySorted;

#if 0  // sort the original array : slowest
    vtkm::cont::ArrayHandle<ValueType> valueArraySorted;
    vtkm::cont::DeviceAdapterAlgorithm<DeviceAdapter>::Copy( keyArray, keyArraySorted );
    vtkm::cont::DeviceAdapterAlgorithm<DeviceAdapter>::Copy( valueArray, valueArraySorted );

    vtkm::cont::DeviceAdapterAlgorithm<DeviceAdapter>::SortByKey( keyArraySorted, valueArraySorted, std::less<KeyType>() ) ;

    print_array("valueArraySorted", valueArraySorted);

    vtkm::cont::ArrayHandleConstant<vtkm::Id> constOneArray(1, valueArray.GetNumberOfValues());
    vtkm::cont::ArrayHandle<vtkm::Id> countArray;

    auto inputZipHandle = vtkm::cont::make_ArrayHandleZip(valueArraySorted, constOneArray);
    auto outputZipHandle = vtkm::cont::make_ArrayHandleZip(sumArray, countArray);

    vtkm::cont::DeviceAdapterAlgorithm<DeviceAdapter>::ReduceByKey( keyArraySorted, inputZipHandle,
                                                                    outputKeyArray, outputZipHandle,
                                                                    vtkm::internal::Add() );
    print_array("sumArray", sumArray);

#else // sort the indexed array: good

    vtkm::cont::ArrayHandleCounting<vtkm::Id> indexArray(0, keyArray.GetNumberOfValues());
    vtkm::cont::ArrayHandle<vtkm::Id> indexArraySorted;

    vtkm::cont::DeviceAdapterAlgorithm<DeviceAdapter>::Copy( keyArray, keyArraySorted );
    vtkm::cont::DeviceAdapterAlgorithm<DeviceAdapter>::Copy( indexArray, indexArraySorted );
    vtkm::cont::DeviceAdapterAlgorithm<DeviceAdapter>::SortByKey( keyArraySorted, indexArraySorted, std::less<KeyType>() ) ;

    //cout << endl;

    auto valueArraySorted
            = vtkm::cont::make_ArrayHandlePermutation( indexArraySorted, valueArray );

    vtkm::cont::ArrayHandleConstant<vtkm::Id> constOneArray(1, valueArray.GetNumberOfValues());
    vtkm::cont::ArrayHandle<vtkm::Id> countArray;
#if 0 // reduce twice
    print_array("keyArraySorted", keyArraySorted);
    print_array("valueArraySorted", valueArraySorted);
    //print_array("constOneArray", constOneArray);
    vtkm::cont::DeviceAdapterAlgorithm<DeviceAdapter>::ReduceByKey( keyArraySorted, valueArraySorted,
                                                                    outputKeyArray, sumArray,
                                                                    vtkm::internal::Add()  );
    vtkm::cont::DeviceAdapterAlgorithm<DeviceAdapter>::ReduceByKey( keyArraySorted, constOneArray,
                                                                    outputKeyArray, countArray,
                                                                    vtkm::internal::Add()  );
    print_array("outputKeyArray", outputKeyArray);
    print_array("sumArray", sumArray);
    print_array("countArray", countArray);
#endif
#if 1 // use zip
    auto inputZipHandle = vtkm::cont::make_ArrayHandleZip(valueArraySorted, constOneArray);
    auto outputZipHandle = vtkm::cont::make_ArrayHandleZip(sumArray, countArray);

    vtkm::cont::DeviceAdapterAlgorithm<DeviceAdapter>::ReduceByKey( keyArraySorted, inputZipHandle,
                                                                    outputKeyArray, outputZipHandle,
                                                                    vtkm::internal::Add() );
#else // debug using serial
    vtkm::cont::DeviceAdapterAlgorithm<vtkm::cont::DeviceAdapterTagSerial>::ReduceByKey( keyArraySorted, valueArraySorted,
                                                                    outputKeyArray, sumArray,
                                                                    vtkm::internal::Add()  );
    vtkm::cont::DeviceAdapterAlgorithm<vtkm::cont::DeviceAdapterTagSerial>::ReduceByKey( keyArraySorted, constOneArray,
                                                                    outputKeyArray, countArray,
                                                                    vtkm::internal::Add()  );
#endif
#endif

#ifndef PROFILING
    std::cout << "AVG Time (s): " << timer.GetElapsedTime() << std::endl;
#endif


    // Using local structure with templates : Only works after c++11
    struct DivideWorklet: public vtkm::worklet::WorkletMapField{
        typedef void ControlSignature(FieldIn<>, FieldIn<>, FieldOut<>);
        typedef void ExecutionSignature(_1, _2, _3);
        VTKM_EXEC_EXPORT void operator()(const ValueType &v, vtkm::Id &count, ValueType &vout) const
        {  vout = v * (1./count);  }
    };

    // get average
    vtkm::worklet::DispatcherMapField<DivideWorklet >().Invoke(sumArray, countArray, outputValueArray);

}

}}
