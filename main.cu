#define BOOST_SP_DISABLE_THREADS

#define vtkRenderingVolume_AUTOINIT 1(vtkRenderingVolumeOpenGL)
#define vtkRenderingContext2D_AUTOINIT 1(vtkRenderingContextOpenGL)
#define vtkRenderingCore_AUTOINIT 3(vtkInteractionStyle,vtkRenderingFreeType,vtkRenderingOpenGL)
//#define vtkRenderingCore_AUTOINIT 4(vtkInteractionStyle,vtkRenderingFreeType,vtkRenderingFreeTypeOpenGL,vtkRenderingOpenGL)

#include <main.hpp>
