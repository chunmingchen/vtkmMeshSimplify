#ifndef VTK_VTKM_H
#define VTK_VTKM_H

#include <vector>
#include <vtkm/cont/DataSet.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkType.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>

#include <vtkTriangle.h>

#define vsp_new(type, name) vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

template<class Real>
vtkm::cont::DataSet vtk2vtkm(vtkSmartPointer<vtkPolyData> data)
{
  typedef vtkm::Vec<Real, 3> PointType;
  vtkm::cont::DataSet ds;

  const int nVerts = data->GetNumberOfPoints();
  std::vector<PointType > xyzVals(nVerts);

  int i;
  for (i=0; i<data->GetPoints()->GetNumberOfPoints(); i++)
    {
      double p[3];
      data->GetPoint(i, p);
      xyzVals[i][0] = p[0];
      xyzVals[i][1] = p[1];
      xyzVals[i][2] = p[2];
    }

  ds.AddField(vtkm::cont::Field("xyz", 0, vtkm::cont::Field::ASSOC_POINTS, xyzVals));
  ds.AddCoordinateSystem(vtkm::cont::CoordinateSystem("xyz"));

  //Set node scalar
  //ds.AddField(Field("nodevar", 1, vtkm::cont::Field::ASSOC_POINTS, vars, nVerts));

  //Set cell scalar
  //vtkm::Float32 cellvar[2] = {100.1f, 100.2f};
  //ds.AddField(Field("cellvar", 1, vtkm::cont::Field::ASSOC_CELL_SET, "cells", cellvar, 2));

  boost::shared_ptr< vtkm::cont::CellSetExplicit<> > cs(
      new vtkm::cont::CellSetExplicit<>("cells", data->GetNumberOfCells()));
  vtkm::cont::ExplicitConnectivity<> &ec = cs->nodesOfCellsConnectivity;

  ec.PrepareToAddCells(data->GetNumberOfCells(), data->GetNumberOfCells()*3);
  for (i=0 ; i<data->GetNumberOfCells(); i++)
    {
      vtkCell *cell = data->GetCell(i);
      ec.AddCell(vtkm::VTKM_TRIANGLE, 3, vtkm::make_Vec<vtkm::Id>(
                   cell->GetPointId(0),
                   cell->GetPointId(1),
                   cell->GetPointId(2)));
    }
  ec.CompleteAddingCells();

  ds.AddCellSet(cs);

  return ds;
}


/// convert to VTK dataset
///
template<class Real>
vtkSmartPointer<vtkPolyData> vtkm2vtk(vtkm::cont::DataSet &ds)
{
  typedef vtkm::Vec<Real, 3> PointType;
  int i;
  assert(ds.GetNumberOfCellSets()>0);
  boost::shared_ptr<vtkm::cont::CellSet> scs = ds.GetCellSet(0);
  vtkm::cont::CellSetExplicit<> *cs =
      dynamic_cast<vtkm::cont::CellSetExplicit<> *>(scs.get());
  VTKM_ASSERT_CONT( cs != NULL );

  vsp_new(vtkCellArray, out_cells);  /// the output cell array
  for (i=0; i < scs->GetNumCells(); i++)
  {
      vtkm::Id3 ids;
      cs->GetNodeToCellConnectivity().GetIndices(i, ids);

      vsp_new(vtkTriangle, triangle);
      triangle->GetPointIds()->SetId(0, ids[0]);
      triangle->GetPointIds()->SetId(1, ids[1]);
      triangle->GetPointIds()->SetId(2, ids[2]);

      out_cells->InsertNextCell(triangle);
  }

  vsp_new(vtkPoints, out_points);
  auto pointarray = ds.GetField("xyz").GetData().CastToArrayHandle(
        PointType(), VTKM_DEFAULT_STORAGE_TAG());

  out_points->SetNumberOfPoints(pointarray.GetNumberOfValues());
  for (i=0; i<pointarray.GetNumberOfValues(); i++)
  {
      PointType p = pointarray.GetPortalControl().Get(i);
      out_points->SetPoint((vtkIdType)i, p[0], p[1], p[2]);
  }

  vsp_new(vtkPolyData, output_data);
  output_data = vtkPolyData::New();
  output_data->SetPoints(out_points);
  output_data->SetPolys(out_cells);

  return output_data;
}

#undef vsp_new

#endif // VTK_VTKM_H

