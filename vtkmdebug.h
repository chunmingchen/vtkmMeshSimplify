#ifndef VTKMDEBUG
#define VTKMDEBUG

#include <iostream>
// debug
template<class T>
void print_array(const char *msg, const T &array)
{
    using namespace std;
#if 1
    cout << msg << "(" << array.GetNumberOfValues() << ")";
    for (int i=0; i<array.GetNumberOfValues(); i++)
    {
        cout << array.GetPortalConstControl().Get(i) << ", ";
    }
    cout << endl;
#endif
}

#endif // VTKMDEBUG

